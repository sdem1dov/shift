Router.configure({
  layoutTemplate: 'index'
});

Router.route('/', {
  name:'index',
  template: 'app'
});

Router.route('/agenda');
Router.route('/commercial');
Router.route('/red');
Router.route('/yellow');
Router.route('/green');
Router.route('/first');
Router.route('/second');
Router.route('/third');
Router.route('/fourth');
Router.route('/fifth');
Router.route('/sixth');
Router.route('/reverse');
Router.route('/parking');
Router.route('/rating');
Router.route('/references');
Router.route('/meteor');
