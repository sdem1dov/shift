import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './index.html';

Template.agenda.rendered = function() {
  $('#fullpage').fullpage({
        verticalCentered: false,
        scrollOverflow: false
      });
}

Template.agenda.events({
  'click section': function(event){
    Router.go('commercial');
  }
});
Template.commercial.events({
  'click div': function(event){
    Router.go('red');
  }
});
Template.red.events({
  'click section': function(event){
    Router.go('yellow');
  }
});
Template.yellow.events({
  'click section': function(event){
    Router.go('green');
  }
});
Template.green.events({
  'click section': function(event){
    Router.go('first');
  }
});
Template.first.events({
  'click section': function(event){
    Router.go('second');
  }
});
Template.second.events({
  'click section': function(event){
    Router.go('third');
  }
});
Template.third.events({
  'click section': function(event){
    Router.go('fourth');
  }
});
Template.fourth.events({
  'click section': function(event){
    Router.go('fifth');
  }
});
Template.fifth.events({
  'click section': function(event){
    Router.go('sixth');
  }
});
Template.sixth.events({
  'click section': function(event){
    Router.go('reverse');
  }
});
Template.reverse.events({
  'click section': function(event){
    Router.go('parking');
  }
});
Template.parking.events({
  'click section': function(event){
    Router.go('rating');
  }
});
Template.rating.events({
  'click .preso-rating': _.debounce(function(){
      Router.go('references')
    }, 2000)
});
Template.references.events({
  'click section': function(event){
    Router.go('meteor');
  }
});
