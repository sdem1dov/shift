# Shi[f]t expectations

## Demo presentation for TechClub weekly event

Should be written in Meteor.js but mainly in pure HTML with a bit of Bootstrap CSS.

Anyway, you'll need Meteor to launch it, so here's the link:
`curl https://install.meteor.com/ | sh`

Or use [official Meteor documentation install page](https://www.meteor.com/install).

When installed, navigate to the project folder and run
`$ meteor`

After some building time on a local machine app should be available in browser at `localhost:3000` URL
